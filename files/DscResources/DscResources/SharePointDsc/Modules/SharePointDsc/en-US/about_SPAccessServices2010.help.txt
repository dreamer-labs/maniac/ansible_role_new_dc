﻿.NAME
    SPAccessServices2010

# Description
    
    **Type:** Distributed
    **Requires CredSSP:** No
    
    This resource is responsible for creating Access Services 2010 Application
    instances within the local SharePoint farm. The resource will provision and
    configure the Access Services 2010 Service Application.
    
    The default value for the Ensure parameter is Present. When not specifying this
    parameter, the service application is provisioned.
    
.PARAMETER Name
    Key - String
    The name of the service application

.PARAMETER ApplicationPool
    Required - String
    The name of the application pool to run the service app in

.PARAMETER Ensure
    Write - String
    Allowed values: Present, Absent
    Present ensures service app exists, absent ensures it is removed

.PARAMETER InstallAccount
    Write - String
    POWERSHELL 4 ONLY: The account to run thsi resource as, use PsDscRunAsCredential if using PowerShell 5


