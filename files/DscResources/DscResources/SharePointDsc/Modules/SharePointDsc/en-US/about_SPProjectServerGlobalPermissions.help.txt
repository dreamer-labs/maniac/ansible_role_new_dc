﻿.NAME
    SPProjectServerGlobalPermissions

# Description
    
    **Type:** Distributed
    **Requires CredSSP:** No
    
    This resource allows you to enforce global permissions in a PWA site for a
    specific project server group or an individual resource.
    
.PARAMETER Url
    Key - string
    The default zone URL of the Project site to manage the group at

.PARAMETER EntityName
    Key - string
    The name of the user or group

.PARAMETER EntityType
    Key - string
    Allowed values: User, Group
    What type of entity are you setting permissions for?

.PARAMETER AllowPermissions
    Write - string
    What permissions should be allowed for this entity?

.PARAMETER DenyPermissions
    Write - string
    What permissions should be denied for this entity?

.PARAMETER InstallAccount
    Write - String
    POWERSHELL 4 ONLY: The account to run this resource as, use PsDscRunAsCredential if using PowerShell 5


.EXAMPLE
    This example shows how to set permissions for a specific resource in a PWA site


Configuration Example 
{
    param(
        [Parameter(Mandatory = $true)]
        [PSCredential]
        $SetupAccount
    )
    Import-DscResource -ModuleName SharePointDsc

    node localhost 
    {
        SPProjectServerGlobalPermissions Permissions
        {
            Url = "http://projects.contoso.com"
            EntityName = "Domain\user"
            EntityType = "User"
            AllowPermissions = @(
                "LogOn",
                "NewTaskAssignment",
                "AccessProjectDataService",
                "ReassignTask",
                "ManagePortfolioAnalyses",
                "ManageUsersAndGroups",
                "ManageWorkflow",
                "ManageCheckIns",
                "ManageGanttChartAndGroupingFormats",
                "ManageEnterpriseCustomFields",
                "ManageSecurity",
                "ManageEnterpriseCalendars",
                "ManageCubeBuildingService",
                "CleanupProjectServerDatabase",
                "SaveEnterpriseGlobal",
                "ManageWindowsSharePointServices",
                "ManagePrioritizations",
                "ManageViews",
                "ContributeToProjectWebAccess",
                "ManageQueue",
                "LogOnToProjectServerFromProjectProfessional",
                "ManageDrivers",
                "ManagePersonalNotifications",
                "ManageServerConfiguration",
                "ChangeWorkflow",
                "ManageActiveDirectorySettings",
                "ManageServerEvents",
                "ManageSiteWideExchangeSync",
                "ManageListsInProjectWebAccess"
            )
            DenyPermissions = @(
                "NewProject"
            )
            PSDscRunAsCredential = $SetupAccount
        }
    }
}


.EXAMPLE
    This example shows how to set permissions for a specific group that exists in a PWA site


Configuration Example 
{
    param(
        [Parameter(Mandatory = $true)]
        [PSCredential]
        $SetupAccount
    )
    Import-DscResource -ModuleName SharePointDsc

    node localhost 
    {
        SPProjectServerGlobalPermissions Permissions
        {
            Url = "http://projects.contoso.com"
            EntityName = "Group Name"
            EntityType = "Group"
            AllowPermissions = @(
                "LogOn",
                "NewTaskAssignment",
                "AccessProjectDataService",
                "ReassignTask",
                "ManagePortfolioAnalyses",
                "ManageUsersAndGroups",
                "ManageWorkflow",
                "ManageCheckIns",
                "ManageGanttChartAndGroupingFormats",
                "ManageEnterpriseCustomFields",
                "ManageSecurity",
                "ManageEnterpriseCalendars",
                "ManageCubeBuildingService",
                "CleanupProjectServerDatabase",
                "SaveEnterpriseGlobal",
                "ManageWindowsSharePointServices",
                "ManagePrioritizations",
                "ManageViews",
                "ContributeToProjectWebAccess",
                "ManageQueue",
                "LogOnToProjectServerFromProjectProfessional",
                "ManageDrivers",
                "ManagePersonalNotifications",
                "ManageServerConfiguration",
                "ChangeWorkflow",
                "ManageActiveDirectorySettings",
                "ManageServerEvents",
                "ManageSiteWideExchangeSync",
                "ManageListsInProjectWebAccess"
            )
            DenyPermissions = @(
                "NewProject"
            )
            PSDscRunAsCredential = $SetupAccount
        }
    }
}


